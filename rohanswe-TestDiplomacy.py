from Diplomacy import diplomacy_solve
from io import StringIO
from unittest import main, TestCase


class TestDiplomacy(TestCase):

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Paris Move Geneva\nC Dallas Move Austin\nD Austin Hold\nE Melbourne Support C\nF Auckland Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A Madrid\nB Geneva\nC Austin\nD [dead]\nE Melbourne\nF Auckland"
        self.assertEqual(w.getvalue(), answer)

    def test_solve_2(self):
        r = StringIO(
            "A Dallas Move Austin\nB Austin Hold\nC SanAntonio Support A\nD Brownsville Support A\nE Rochester Support B\nF Lagos Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A [dead]\nB [dead]\nC SanAntonio\nD Brownsville\nE Rochester\nF Lagos"
        self.assertEqual(w.getvalue(), answer)

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Move NewYork\nB Toronto Move Madrid\nC NewYork Move Eugene\nD Eugene Move Toronto")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A NewYork\nB Madrid\nC Eugene\nD Toronto"
        self.assertEqual(w.getvalue(), answer)

    def test_solve_4(self):
        r = StringIO("A Dallas Move Austin\nB Austin Hold\nC SanAntonio Support A\nD Brownsville Support A\nE Rochester Support B\nF Lagos Support B\nG Madrid Move Lagos")
        w = StringIO()
        diplomacy_solve(r, w)
        answer = "A Austin\nB [dead]\nC SanAntonio\nD Brownsville\nE Rochester\nF [dead]\nG [dead]"
        self.assertEqual(w.getvalue(), answer)


if __name__ == "__main__":  # pragma: no cover
    main()
